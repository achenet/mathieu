use std::io::Error;
mod lexer;
mod parser;

fn main() {
    println!("Hello, world!");
    println!("Mathieu turns text stream into Lisp syntax trees");
    let input = get_input().unwrap();
    let tokens = lexer::tokenize(input).unwrap();
    println!("tokens:{:?}", tokens);
    let ast = parser::parse(tokens).unwrap();
    println!("ast:{:?}", ast);
}

fn get_input() -> Result<String, Error> {
    Ok("( hi )".to_string())
}
