use std::io::Error;

#[derive(Debug, Eq, PartialEq)]
pub enum Token {
    LParen,
    RParen,
    Symbol(String),
}

pub fn tokenize(input: String) -> Result<Vec<Token>, Error> {
    let split: Vec<&str> = input.split(' ').collect();
    println!("split:{:?}", split);
    let mut out = vec![];
    for s in split {
        out.push(to_token(s).unwrap());
    }
    Ok(out)
}

fn to_token(input: &str) -> Result<Token, Error> {
    match input {
        "(" => {
            return Ok(Token::LParen);
        }
        ")" => {
            return Ok(Token::RParen);
        }
        _ => {
            return Ok(Token::Symbol(input.to_string()));
        }
    }
}
