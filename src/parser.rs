use crate::lexer::Token;
use std::io::{Error, ErrorKind::Other};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Value {
    Symbol(String),
    List(Vec<Box<Value>>),
    Nil,
}

impl Value {
    fn new(parent: Value) -> Self {
        if parent == Value::Nil {
            return Value::List(vec![]);
        }
        Value::List(vec![Box::new(parent)])
    }
}

#[derive(Debug, Clone)]
pub struct Node {
    parent: Box<Option<Node>>,
    value: Value,
}

impl Node {
    fn new() -> Self {
        Node {
            parent: Box::new(None),
            value: Value::Nil,
        }
    }

    fn new_from_value_with_parent(parent: Box<Option<Node>>, value: Value) -> Self {
        Node { parent, value }
    }
}

pub fn parse(tokens: Vec<Token>) -> Result<Vec<Value>, Error> {
    let root = Node::new();
    let mut current = Box::new(Some(root));
    for t in tokens {
        match t {
            Token::LParen => {
                // new list
                let new_node =
                    Node::new_from_value_with_parent(current.clone(), Value::List(vec![]));
                current = Box::new(Some(new_node));
            }
            Token::RParen => {
                // finish node
                let current_node = *current.clone();
                match current_node {
                    Some(node) => {
                        current = node.parent;
                    }
                    None => {
                        return Err(Error::new(Other, "no parent node"));
                    }
                }
            }
            Token::Symbol(s) => {
                // so something with s

                match *current.clone() {
                    Some(node) => match node.value {
                        Value::List(mut l) => {
                            l.push(Box::new(Value::Symbol(s)));
                        }
                        _ => return Err(Error::new(Other, "node value not list")),
                    },
                    None => {
                        return Err(Error::new(Other, "node option value is None"));
                    }
                }
            }
            _ => {
                return Err(Error::new(Other, "invalid token"));
            }
        }
    }
    let out = vec![];
    Ok(out)
}
